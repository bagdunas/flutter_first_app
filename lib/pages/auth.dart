import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import 'package:flutter_first_app/widgets/helpers/ensure-visible.dart';
import 'package:flutter_first_app/scoped_model/main.dart';

import '../models/auth.dart';


class AuthPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AuthPageState();
  }
}

class _AuthPageState extends State<AuthPage> {
  final Map<String, dynamic> _formData = {
    'email': null,
    'password': null,
    'acceptTerms': false,
  };
  final GlobalKey<FormState> _loginFormKey = GlobalKey<FormState>();
  final _emailFocusNode = FocusNode();
  final _passwordFocusNode = FocusNode();
  final _passwordConfirmFocusNode = FocusNode();
  final TextEditingController _passwordTextController = TextEditingController();
  AuthMode _authMode = AuthMode.Login;

  BoxDecoration _buildBackgroundImage(){
    return BoxDecoration(
      image: DecorationImage(
          colorFilter: ColorFilter.mode(Colors.white.withOpacity(0.4), BlendMode.dstATop),
          image: AssetImage('assets/login-bg.jpg'),
          fit: BoxFit.cover),
    );
  }

  Widget _buildLoginTextField(){
    return Container(
      padding: EdgeInsets.only(bottom: 10),
      child: EnsureVisibleWhenFocused(
        focusNode: _emailFocusNode,
        child: TextFormField(
          focusNode: _emailFocusNode,
          decoration: InputDecoration(labelText: 'E-Mail'),
          keyboardType: TextInputType.emailAddress,
          validator: (String value) {
            if (value.isEmpty || !RegExp(r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$").hasMatch(value)) {
              return 'Please enter valid email';
            }
          },
          onSaved: (String value) {
            _formData['email'] = value;
          },
        ),
      ),
    );
  }

  Widget _buildPasswordTextField(){
    return Container(
      padding: EdgeInsets.only(bottom: 10),
      child: EnsureVisibleWhenFocused(
        focusNode: _passwordFocusNode,
        child: TextFormField(
          obscureText: true,
          controller: _passwordTextController,
          focusNode: _passwordFocusNode,
          decoration: InputDecoration(labelText: 'Password'),
          validator: (String value) {
            if (value.isEmpty || value.length <= 5) {
              return 'Password is required and should be 5+ characters long.';
            }
          },
          onSaved: (String value) {
            _formData['password'] = value;
          },
        ),
      ),
    );
  }

  Widget _buildPasswordConfirmTextField(){
    return Container(
      padding: EdgeInsets.only(bottom: 10),
      child: EnsureVisibleWhenFocused(
        focusNode: _passwordConfirmFocusNode,
        child: TextFormField(
          focusNode: _passwordConfirmFocusNode,
          obscureText: true,
          decoration: InputDecoration(labelText: 'Confir Password'),
          validator: (String value) {
            if (_passwordTextController.text != value) {
              return 'Password do not match';
            }
          },
        ),
      ),
    );
  }

  Widget _buildAcceptTerms(){
    return Container(
        padding: EdgeInsets.only(bottom: 20),
        child: SwitchListTile(
          title: Text('Accept Terms'),
          value: _formData['acceptTerms'],
          onChanged: (bool value) {
            setState(() {
              _formData['acceptTerms'] = value;
            });
          },
        )
    );
  }

  Widget _buildSubmitButton() {
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model){
          return model.isLoading ?
            Center(child: CircularProgressIndicator(),)
            :
            Container(
                padding: EdgeInsets.only(bottom: 20),
                child: RaisedButton(
                  child: Text('Save'),
                  color: Theme.of(context).accentColor,
                  onPressed: () => _submitForm(model.authenticate),
                )
            );
        }
    );
  }

  void _showErrorDialog(String message){
    showDialog(context: context, builder: (BuildContext context) {
      return AlertDialog(
        title: Text('An Error Occurred!'),
        content: Text(message),
        actions: <Widget>[
          FlatButton(child: Text('Okey'), onPressed: (){
            Navigator.of(context).pop();
          },)
        ],
      );
    });
  }

  void _submitForm(Function authenticate) async {
    if (!_loginFormKey.currentState.validate() || !_formData['acceptTerms']) {
      return;
    }
    _loginFormKey.currentState.save();
    Map<String, dynamic> response = await authenticate(_formData['email'], _formData['password'], _authMode);
    if(response['success']){
//      Navigator.pushReplacementNamed(context, '/');
    } else {
      _showErrorDialog(response['message']);
    }
  }

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 550.0 ? 500.0 : deviceWidth * 0.9;
    return Scaffold(
        appBar: AppBar(
          title: Text("Login"),
        ),
        body: Container(
            decoration: _buildBackgroundImage(),
            child: Center(
              child: Container(
                width: targetWidth,
                child: Form(
                  key: _loginFormKey,
                  child: ListView(
                    shrinkWrap: true,
                    padding: EdgeInsets.all(20),
                    children: <Widget>[
                      _buildLoginTextField(),
                      SizedBox(
                        height: 10,
                      ),
                      _buildPasswordTextField(),
                      _authMode == AuthMode.Signup ? _buildPasswordConfirmTextField() : Container(),
                      _buildAcceptTerms(),
                      SizedBox(
                        height: 10,
                      ),
                      FlatButton(
                        child: Text('Switch to ${_authMode == AuthMode.Login ? 'Signup' : 'Login'}'),
                        onPressed: (){
                          setState(() {
                            _authMode = _authMode == AuthMode.Login ? AuthMode.Signup : AuthMode.Login;
                          });
                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      _buildSubmitButton(),
                    ]
                  )
                )
              )
            )
        )
    );
  }
}
