import 'package:flutter/material.dart';
import 'package:flutter_first_app/scoped_model/main.dart';
import 'package:flutter_first_app/widgets/ui_elements/logout_list_tile.dart';
import './product_edit.dart';
import './product_list.dart';
// import './products.dart';

class ProductAdminPage extends StatelessWidget {
  final MainModel model;

  ProductAdminPage(this.model);

  Widget _buildSideDrawer(BuildContext context){
    return Drawer(
      child: Column(children: <Widget>[
        AppBar(
          automaticallyImplyLeading: false,
          title: Text('Chose'),
        ),
        ListTile(
          leading: Icon(Icons.list),
          title: Text('Products List'),
          onTap: () {
            Navigator.pushReplacementNamed(context, '/');
          },
        ),
        Divider(),
        LogoutListTile()
      ],),
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        drawer: _buildSideDrawer(context),
        appBar: AppBar(
          title: Text("Manage Products"),
          bottom: TabBar(tabs: <Widget>[
            Tab(
              icon: Icon(Icons.create),
              text: 'Create Product',
            ),
            Tab(
              icon: Icon(Icons.list),
              text: 'My Products',
            )
          ],),
        ),
        body: TabBarView(
          children: <Widget>[
            ProductEditPage(model),
            ProductListPage(model)
          ],
        ),
        // bottomNavigationBar: ,
      ),
    );
  }
}