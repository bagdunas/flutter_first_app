import 'package:flutter/material.dart';

class AuthPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AuthPageState();
  }
}

class _AuthPageState extends State<AuthPage> {
  String mail, pass;

  Widget _addnewTextField(String hint, TextInputType inputType, String output,
      Icon icon, bool password) {
    return Container(
      margin: EdgeInsets.all(10),
      child: TextField(
        obscureText: password,
        autofocus: false,
        autocorrect: false,
        keyboardType: inputType,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10),
          hintText: hint,
          prefixIcon: icon,
          hintStyle: TextStyle(color: Colors.grey),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(40)),
              borderSide: BorderSide(
                  color: Theme.of(context).primaryColor,
                  style: BorderStyle.solid,
                  width: 1)),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(40)),
              borderSide: BorderSide(
                  color: Theme.of(context).primaryColor,
                  style: BorderStyle.solid,
                  width: 1)),
        ),
        onChanged: (String value) {
          setState(() {
            switch (output) {
              case 'email':
                mail = value;
                break;
              case 'pass':
                pass = value;
                break;
            }
          });
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: <Widget>[
        Positioned.fill(child: Image.network('https://buzzfollow.herokuapp.com/img/login-bg.jpg',fit: BoxFit.fill,)),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            _addnewTextField('Write Your Mail', TextInputType.text, 'email',
                Icon(Icons.mail), false),
            _addnewTextField('Write Your Password', TextInputType.text,
                'password', Icon(Icons.lock), true),
            RaisedButton(
              onPressed: () {
                Navigator.pushReplacementNamed(context, '/home');
              },
              color: Theme.of(context).primaryColor,
              child: Text(
                "Login",
                style: TextStyle(color: Colors.white),
              ),
              elevation: 10,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10))),
            ),
          ],
        )
      ]),
    );
  }
}
