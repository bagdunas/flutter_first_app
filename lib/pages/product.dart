import 'package:flutter/material.dart';
import 'package:flutter_first_app/models/product.dart';
import 'package:flutter_first_app/scoped_model/main.dart';
import 'package:flutter_first_app/widgets/products/price_tag.dart';
import 'dart:async';
import '../widgets/ui_elements/title_default.dart';

class ProductPage extends StatelessWidget {
  final MainModel model;

  ProductPage(this.model);

  _showWarningDialog(BuildContext context){
    showDialog(
      context: context, 
      builder: (BuildContext context){
        return AlertDialog(
            title: Text('Are you sure?'), 
            content: Text('This action acnot be undone!'),
            actions: <Widget>[
              FlatButton(
                child: Text('DISCARD'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                child: Text('CONTINUE'),
                color: Colors.redAccent,
                textColor: Colors.white,
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.pop(context, true);
                },
              )
            ],
        );
      }
    );
  }

  Widget _buildPageContext(BuildContext context) {
    final Product product = model.selectedProduct;
    return Scaffold(
        appBar: AppBar(
          title: Text(product.title),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            FadeInImage(
              image: NetworkImage(product.image),
              height: 300.00,
              fit: BoxFit.cover,
              placeholder: AssetImage('assets/placeholder600x400.jpg'),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child: TitleDefault(product.title)
                  ),
                  SizedBox(width: 8.0),
                  PriceTag(product.price.toString()),
                ],
              ),
            ),
            Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.all(10),
              child: Text(product.description),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: RaisedButton(
                color: Theme.of(context).accentColor,
                child: Text('DELETE'),
                onPressed: () => _showWarningDialog(context),
              ),
            ),
          ],
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () { // On press return clear product selections. Should be FUTURE
          model.selectedProductId = null;
          Navigator.pop(context, false); // Replace rooting;
          return Future.value(false);
        },
        child: _buildPageContext(context)
    );
  }
}
