import 'package:flutter/material.dart';
import 'package:flutter_first_app/models/location_data.dart';
import 'package:flutter_first_app/scoped_model/main.dart';
import 'dart:async';

import 'package:flutter_first_app/models/product.dart';
import 'package:flutter_first_app/widgets/form_input/location.dart';
import 'package:flutter_first_app/widgets/ui_elements/title_default.dart';
import 'package:flutter_first_app/widgets/helpers/ensure-visible.dart';

class ProductEditPage extends StatefulWidget {
  final MainModel model;

  ProductEditPage(this.model);

  @override
  State<StatefulWidget> createState() {
    return _ProductEditPageState(model);
  }
}

class _ProductEditPageState extends State<ProductEditPage> {
  final MainModel model;
  final Map<String, dynamic> _formData = {
    'title': null,
    'description': null,
    'price': null,
    'image': 'assets/kod1.jpg',
    'location': null
  };
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _titleFocusNode = FocusNode();
  final _descriptionFocusNode = FocusNode();
  final _priceFocusNode = FocusNode();

  _ProductEditPageState(this.model);

  Widget _buildTitleTextField(Product product) {
    return Container(
      padding: EdgeInsets.only(bottom: 10),
      child: EnsureVisibleWhenFocused(
        focusNode: _titleFocusNode,
        child: TextFormField(
          focusNode: _titleFocusNode,
          initialValue: product == null ? '' : product.title,
          decoration: InputDecoration(labelText: 'Product Title'),
          validator: (String value) {
  //          if(value.trim().length <= 0){
            if (value.isEmpty || value.length <= 5) {
              return 'Title is required and should be 5+ characters long.';
            }
          },
          onSaved: (String value) {
            _formData['title'] = value;
          },
        ),
      ),
    );
  }

  Widget _buildDescriptionTextField(Product product) {
    return Container(
      padding: EdgeInsets.only(bottom: 10),
      child: EnsureVisibleWhenFocused(
        focusNode: _descriptionFocusNode,
        child: TextFormField(
          focusNode: _descriptionFocusNode,
          initialValue:
              product == null ? '' : product.description,
          keyboardType: TextInputType.multiline,
          maxLines: 4,
          validator: (String value) {
            if (value.isEmpty || value.length <= 10) {
              return 'Description is required and should be 10+ characters long.';
            }
          },
          decoration: InputDecoration(labelText: 'Product Description'),
          onSaved: (String value) {
            _formData['description'] = value;
          },
        ),
      ),
    );
  }

  Widget _buildPriceTextField(Product product) {
    return Container(
      padding: EdgeInsets.only(bottom: 20),
      child: EnsureVisibleWhenFocused(
        focusNode: _priceFocusNode,
        child: TextFormField(
          focusNode: _priceFocusNode,
          initialValue:
              product == null ? '' : product.price.toString(),
          keyboardType: TextInputType.number,
          decoration: InputDecoration(labelText: 'Product Price'),
          validator: (String value) {
            if (value.isEmpty ||
                !RegExp(r'^(?:[1-9]\d*|0)?(?:\.\d+)?$').hasMatch(value)) {
              return 'Price is required and should be number.';
            }
          },
          onSaved: (String value) {
            _formData['price'] =
                double.parse(value.replaceFirst(RegExp(r','), '.'));
          },
        ),
      ),
    );
  }

  Widget _buildSubmitButton() {
    return model.isLoading ?
      Center(child: CircularProgressIndicator(),)
      :
      Container(
        padding: EdgeInsets.only(bottom: 20),
        child: RaisedButton(
          child: Text('Save'),
          onPressed: () => _submitForm(model.addProduct, model.updateProduct, model.selectProduct, model.selectedProductIndex),
        ),
      );
  }

  Widget _buildPageContent(BuildContext context, Product product, {bool productEdition = false}){
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 550.0 ? 500.0 : deviceWidth * 0.95;
    final double targetPadding = deviceWidth - targetWidth;
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Container(
        width: targetWidth,
        child: Form(
          key: _formKey,
          child: ListView(
            padding: EdgeInsets.symmetric(
                horizontal: targetPadding / 2, vertical: 20.0),
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(bottom: 10.0),
                child: productEdition ? TitleDefault('Edit product') : TitleDefault('Create new product'),
              ),
              _buildTitleTextField(product),
              _buildDescriptionTextField(product),
              _buildPriceTextField(product),
              SizedBox(height: 10.0,),
              LocationInput(_setLocation, location: product != null ? product.location : null),
              SizedBox(height: 20.0,),
              _buildSubmitButton(),
              SizedBox(
                height: 20,
              ), // Pusty element o wysokosci 10px
            ],
          ),
        ),
      ),
    );
  }

  void _setLocation(Location location){
    _formData['location'] = location;
  }


  void _submitForm(Function addProduct, Function updateProduct, Function setSelectedProduct, [int selectedProductIndex]) {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();
    print(_formData);
    if(selectedProductIndex == -1){
      addProduct(
        title: _formData['title'],
        description: _formData['description'],
        price: _formData['price'],
        image: _formData['image'],
        location: _formData['location']
      ).then((bool success) {
        if(success){
          Navigator.pushReplacementNamed(context, '/').then((_){
            setSelectedProduct(null);
          });
        } else {
          showDialog(
            context: context,
            builder: (BuildContext context){
              return AlertDialog(
                title: Text('Somthing went wrong!'),
                content: Text('Please try again!'),
                actions: <Widget>[
                  FlatButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: Text('OK'),
                  )],
              );
          });
        }
      });
    } else {
      updateProduct(
          title: _formData['title'],
          description: _formData['description'],
          price: _formData['price'],
          image: _formData['image'],
          location: _formData['location']
      ).then((_) =>  Navigator.pushReplacementNamed(context, '/').then((_){
        setSelectedProduct(null);
      }));
    }
  }

  Widget _buildPageCondition(BuildContext context){
    if(model.selectedProductId == null){
      return _buildPageContent(context, model.selectedProduct);
    }
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Product'),
      ),
      body: Center(child: _buildPageContent(context, model.selectedProduct, productEdition: true)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () { // On press return clear product selections. Should be FUTURE
          model.selectedProductId = null;
          return Future.value(true);
        },
        child: _buildPageCondition(context)
    );
  }
}
