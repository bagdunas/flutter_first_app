import 'package:flutter/material.dart';
import 'package:flutter_first_app/scoped_model/main.dart';
import 'package:flutter_first_app/widgets/ui_elements/logout_list_tile.dart';
import 'package:scoped_model/scoped_model.dart';
import '../widgets/products/products.dart';

// import './product_admin.dart';

class ProductsPage extends StatefulWidget {
  final MainModel model;

  ProductsPage(this.model);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ProductsPageState();
  }

}


class _ProductsPageState extends State<ProductsPage> {

  @override
  initState(){
    widget.model.fetchProducts();
    super.initState();
  }

  Widget _buildSideDrawer(BuildContext context){
    return Drawer(
      child: Column(children: <Widget>[
        AppBar(
          automaticallyImplyLeading: false,
          title: Text('Chose'),
        ),
        ListTile(
          leading: Icon(Icons.edit),
          title: Text('Manage Products'), onTap: (){
          Navigator.pop(context, true);
          Navigator.pushReplacementNamed(context, '/admin');
        },),
        Divider(),
        LogoutListTile()
      ],),
    );
  }

  Widget _buildProductList (BuildContext context){
    return ScopedModelDescendant<MainModel> ( builder: (BuildContext context, Widget child, MainModel model){
      Widget content = Center(child: Text("No products found!"),);
      if(model.displayedProducts.length > 0 && !model.isLoading){
        content = Products();
      } else if (model.isLoading){
        content = Center(child: CircularProgressIndicator(),);
      }
      return RefreshIndicator(child: content, onRefresh: () {
        return model.fetchProducts(triggerLoader: false);
      });
    },);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: _buildSideDrawer(context),
        appBar: AppBar(
          title: Text("EasyList"),
          actions: <Widget>[
            ScopedModelDescendant<MainModel>(
              builder: (BuildContext context, Widget child, MainModel model) {
                return IconButton(
                  icon: Icon( model.showFavorites ? Icons.favorite : Icons.favorite_border),
                  onPressed: () {
                    model.toggleDisplayMode();
                  },
                );
              },
            ),
          ],
        ),
        body: _buildProductList(context)
    );
  }
}