import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_first_app/widgets/helpers/ensure-visible.dart';
import 'package:flutter_first_app/widgets/helpers/static_map_provider.dart';
import '../../models/location_data.dart';

class LocationInput extends StatefulWidget {
  final Function _setLocation;
  Location location;

  LocationInput(this._setLocation, {this.location});

  @override
  State<StatefulWidget> createState() {
    return _LocationInputState();
  }
}

class _LocationInputState extends State<LocationInput> {
  final _locationFocusNode = FocusNode();
  final TextEditingController _locationController = TextEditingController();

  @override
  void initState(){
    _locationFocusNode.addListener(_updateLocation);
    if(widget.location != null && widget.location.address != null){
      _locationController.text = widget.location.address;
    }
    super.initState();
  }

  @override
  void dispose() {
    _locationFocusNode.removeListener(_updateLocation);
    super.dispose();
  }

  void _updateLocation(){
    if(!_locationFocusNode.hasFocus){
      getLocation(_locationController.text);
      print('NOT FOCUSED ${_locationController.text}');
    }
  }

  void getLocation(String address) async {
    if(address.isNotEmpty){
      final Uri uri = new Uri.https('maps.googleapis.com', '/maps/api/geocode/json', {
        'key': 'AIzaSyDaSWMCP55VNMnvLmFHlLPra_vByZMpyjg',
        'address': address
      });
      http.Response response = await http.get(uri);
      final decodeResponse = jsonDecode(response.body);
      if(decodeResponse['results'].length > 0){
        final String formattedAddress = decodeResponse['results'][0]['formatted_address'];
        final coords = decodeResponse['results'][0]['geometry']['location'];
        final location = Location(
            latitude: coords['lat'],
            longitude: coords['lng'],
            address: formattedAddress
        );
        widget._setLocation(location);
        setState(() {
          _locationController.text = location.address;
          widget.location = location;
        });
      } else if(decodeResponse['status'] == 'OVER_QUERY_LIMIT'){
        print("Somthing went wrong: $decodeResponse");
        widget._setLocation(Location(latitude: 51.779974, longitude: 19.431796, address: address));
      }
    } else {
      widget._setLocation(null);
      widget.location = null;
    }
  }


  Widget _buildLocationTextField() {
    return Container(
      padding: EdgeInsets.only(bottom: 10),
      child: EnsureVisibleWhenFocused(
        focusNode: _locationFocusNode,
        child: TextFormField(
          focusNode: _locationFocusNode,
          controller: _locationController,
          decoration: InputDecoration(labelText: 'Location Adress'),
          validator: (String value) {
            if (value.isEmpty) {
              return 'No valid location found.';
            }
          }
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context){
    return Column(
      children: <Widget>[
        _buildLocationTextField(),
        SizedBox(height: 10.0,),
        widget.location != null ? StaticMapsProvider(location: widget.location) : Container(),
//        SizedBox(
//          width: 300.0,
//          height: 200.0,
//          child: GoogleMap(
//            onMapCreated: _onMapCreated,
//            initialCameraPosition: CameraPosition(
//              target: _center,
//              zoom: 11.0,
//            ),
//          ),
//        )
      ],
    );
  }
}