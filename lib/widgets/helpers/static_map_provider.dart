import 'package:flutter/material.dart';
import 'package:flutter_first_app/models/location_data.dart';

class StaticMapsProvider extends StatelessWidget {
  final String googleMapsApiKey;
  final Location location;
  final int zoomLevel;
  final int width;
  final int height;

  StaticMapsProvider({
    this.googleMapsApiKey,
    this.location = const Location(latitude: 51.779974, longitude: 19.431796),
    this.width = 600,
    this.height = 400,
    this.zoomLevel = 15
  });

  Uri _buildUrl() {
    var baseUri;
    if(googleMapsApiKey != null) {
      // https://maps.googleapis.com/maps/api/staticmap?size=640x440&center=37.0902%2C-95.7192&zoom=4&key=AIzaSyDaSWMCP55VNMnvLmFHlLPra_vByZMpyjg
      baseUri = new Uri(
          scheme: 'https',
          host: 'maps.googleapis.com',
          path: '/maps/api/staticmap',
          queryParameters: {
            'size': '${width}x$height',
            'center': location.coordsToGoogleString(),
            'zoom': '$zoomLevel',
            'key': '$googleMapsApiKey'
          });
    } else {
      // https://static-maps.yandex.ru/1.x/?lang=en-US&ll=-73.7638,42.6564&z=13&l=trf,map,skl,sat&size=600,400
      baseUri = new Uri(
          scheme: 'https',
          host: 'static-maps.yandex.ru',
          path: '/1.x/',
          queryParameters: {
            'lang': 'en-US',
            'll': location.coordsToYandexString(),
            'z': '$zoomLevel',
            'l': 'map',
            'size': '$width,$height',
            'pt': '${location.coordsToYandexString()},round'
          });
    }
    return baseUri;
  }

  Widget build(BuildContext context) {
    return Image.network(_buildUrl().toString());
  }
}