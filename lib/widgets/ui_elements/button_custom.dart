import 'package:flutter/material.dart';

class ButtonCustom extends StatelessWidget {
  final String text;
  final Function onTap;

  ButtonCustom(this.text, this.onTap);

  @override
  Widget build(BuildContext context){
    return GestureDetector( // TODO: GestureDetector - possible to add event to any element current simple Container
      onTap: onTap,
      child: Container(
        alignment: Alignment.center,
        color: Colors.red,
        padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 5.0),
        child: Text(text, style: TextStyle(color: Colors.white)),
      ),
    );
  }
}