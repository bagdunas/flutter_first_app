import 'package:flutter/material.dart';
import 'package:flutter_first_app/scoped_model/main.dart';
import '../../models/product.dart';
import './price_tag.dart';
import '../ui_elements/title_default.dart';

class ProductCard extends StatelessWidget {
  final MainModel model;
  final Product product;
  final int productIndex;

  ProductCard(this.model, this.product, this.productIndex);

  Widget _buildActionsButtons(BuildContext context) {
    return ButtonBar(
      alignment: MainAxisAlignment.center,
      children: <Widget>[
        IconButton(
          icon: Icon(Icons.info),
          color: Theme
              .of(context)
              .accentColor,
          onPressed: () =>
              Navigator.pushNamed<bool>(context, '/product/${product.id}'),
        ),
        IconButton(
          icon: Icon( model.displayedProducts[productIndex].isFavorite ? Icons.favorite : Icons.favorite_border),
          color: Colors.red,
          onPressed: () {
            model.selectProduct(model.displayedProducts[productIndex].id);
            model.toggleProductFavoriteStatus();
          },
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context){
    return Card(
        child: Column(
          children: <Widget>[
            FadeInImage(
              image: NetworkImage(product.image),
              height: 300.00,
              fit: BoxFit.cover,
              placeholder: AssetImage('assets/placeholder600x400.jpg'),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child: TitleDefault(product.title)
                  ),
                  SizedBox(width: 8.0),
                  PriceTag(product.price.toString()),
                ],
              ),
            ),
            SizedBox(height: 8.0),
            DecoratedBox(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.0),
                border: Border.all(
                    color: Theme.of(context).accentColor,
                    width: 1,
                    style: BorderStyle.solid),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 3.0),
                child: Text(product.description),
              ),
            ),
            Container(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 3.0),
                child: Text(product.userEmail),
              ),
            ),
            _buildActionsButtons(context)
          ],
        )
    );
  }
}