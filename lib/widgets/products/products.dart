import 'package:flutter/material.dart';
import 'package:flutter_first_app/scoped_model/main.dart';
import 'package:scoped_model/scoped_model.dart';

import 'package:flutter_first_app/models/product.dart';
import 'package:flutter_first_app/widgets/products/product_card.dart';

// import './pages/product.dart';

class Products extends StatelessWidget {

  Widget _buildProductList(MainModel model, List<Product> products) {
    Widget productCarts;
    if (products.length > 0) {
      // ListView() render elements small amount;
      // ListView.builder() render elements with load more.
      productCarts = ListView.builder(
        itemBuilder: (BuildContext context, int index) =>  ProductCard(model, products[index], index),
        itemCount: products.length,
      );
    } else {
      productCarts = Center(
        child: Text("Please provide new products"),
      );
    }
    return productCarts; // Empty container Container()
  }

  @override
  Widget build(BuildContext context) {
    print('[Products Widget] build()');
    return ScopedModelDescendant<MainModel>(builder: (BuildContext context, Widget child, MainModel model){
      return _buildProductList(model, model.displayedProducts);
    });
  }
}
