import 'dart:convert';
import 'dart:async';
import 'package:flutter_first_app/models/location_data.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter_first_app/models/product.dart';
import 'package:flutter_first_app/models/user.dart';
import 'package:rxdart/subjects.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../models/auth.dart';

mixin ConnectedProductsModel on Model {
  List<Product> _products = [];
  User _authenticatedUser;
  String _selectedProductId;
  bool _isLoading = false;
}

mixin UserModel on ConnectedProductsModel {
  Timer _authTimer;
  PublishSubject<bool> _userSubject = PublishSubject();

  User get user {
    return _authenticatedUser;
  }

  PublishSubject<bool> get userSubject {
    return _userSubject;
  }

  Future<Map<String, dynamic>> authenticate(String email, String password, [AuthMode mode = AuthMode.Login]) async {
    _isLoading = true;
    notifyListeners();
    final Map<String, dynamic> authData = {
      'email': email,
      'password':	password,
      'returnSecureToken': true
    };
    http.Response response;
    if(mode == AuthMode.Login){
      response = await http.post(
          'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyDAxztaZbqC5wCPFpSXCYdQ0ejxxoecwcc',
          body: jsonEncode(authData),
          headers: {'Content-Type': 'application/json'}
      );
    } else {
      response = await http.post(
          'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyDAxztaZbqC5wCPFpSXCYdQ0ejxxoecwcc',
          body: jsonEncode(authData),
          headers: {'Content-Type': 'application/json'}
      );
    }
    final Map<String, dynamic> responseData = jsonDecode(response.body);
    bool hasError = true;
    String message = 'Something went wrong.';
    if(responseData.containsKey('idToken')){
      hasError = false;
      message = 'Authentication succeeded!';
      _authenticatedUser = new User(
          id: responseData['localId'],
          email: responseData['email'],
          idToken: responseData['idToken']
      );
      setAuthTimeout(int.parse(responseData['expiresIn']));
      final DateTime now = DateTime.now();
      final DateTime expireTime = now.add(Duration(seconds: int.parse(responseData['expiresIn'])));
      _userSubject.add(true);
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('idToken', responseData['idToken']);
      prefs.setString('userEmail', responseData['email']);
      prefs.setString('userId', responseData['localId']);
      prefs.setString('expireTime', expireTime.toIso8601String());
    } else if (responseData['error']['message'] == 'EMAIL_NOT_FOUND') {
      message = 'There is no user record corresponding to this identifier.';
    } else if (responseData['error']['message'] == 'INVALID_PASSWORD') {
      message = 'The password is invalid.';
    } else if (responseData['error']['message'] == 'EMAIL_EXISTS') {
      message = 'This email already exists.';
    }
    _isLoading = false;
    notifyListeners();
    return {'success': !hasError, 'message': message};
  }

   void autoAuthenticate() async {
     final SharedPreferences prefs = await SharedPreferences.getInstance();
     final String idToken = prefs.getString('idToken');
     final String expireTimeString = prefs.getString('expireTime');
     if(idToken != null){
       final DateTime now = DateTime.now();
       final parseTimeExpiry = DateTime.parse(expireTimeString);
       if (parseTimeExpiry.isBefore(now)) {
         _authenticatedUser = null;
         notifyListeners();
         return;
       }
       final String email = prefs.getString('userEmail');
       final String id = prefs.getString('userId');
       final int tokenLifeSpan = parseTimeExpiry.difference(now).inSeconds;
       _authenticatedUser = new User(
           id: id,
           email: email,
           idToken: idToken
       );
       _userSubject.add(true);
       setAuthTimeout(tokenLifeSpan);
       notifyListeners();
     }
  }

  void logout() async {
    _authenticatedUser = null;
    _authTimer.cancel();
    _userSubject.add(false);
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('idToken');
    prefs.remove('userEmail');
    prefs.remove('userId');
  }

  void setAuthTimeout (int time){
    _authTimer = Timer(Duration(seconds: time), logout);
  }
}

mixin ProductsModel on ConnectedProductsModel {
  bool _showFavorites = false;

  List<Product> get allProducts {
    return List.from(_products);
  }

  List<Product> get displayedProducts {
    if(_showFavorites){
      return _products.where((Product product) => product.isFavorite).toList();
    }
    return List.from(_products);
  }

  bool get showFavorites {
    return _showFavorites;
  }

  Product get selectedProduct {
    if (_selectedProductId == null) {
      return null;
    }
    return _products.firstWhere((Product product) {
      return product.id == _selectedProductId;
    });
  }

  String get selectedProductId {
    return _selectedProductId;
  }

  set selectedProductId(String id) {
    _selectedProductId = id;
  }

  int get selectedProductIndex {
    return _products.indexWhere((Product product){
      return product.id == _selectedProductId;
    });
  }

  // ASYNC example:
  Future<bool> addProduct({String title, String description, String image, double price, Location location}) async {
    _isLoading = true;
    notifyListeners();
    final Map<String, dynamic> productData = {
      'title': title,
      'description': description,
      'image': 'https://dummyimage.com/600x400/e0a2e0/1220e3.png',
      'price': price,
      'loc_lat': location.latitude,
      'loc_lng': location.longitude,
      'loc_address': location.address,
      'userEmail': _authenticatedUser.email,
      'userId': _authenticatedUser.id
    };
    try {
      final http.Response response = await http.post(
          'https://flutter-first-app-55155.firebaseio.com/products.json?auth=${_authenticatedUser.idToken}', body: jsonEncode(productData));
      if (response.statusCode != 200 && response.statusCode != 201) {
        _isLoading = false;
        notifyListeners();
        return false;
      }
      final Map<String, dynamic> responseData = jsonDecode(response.body);
      final newProduct = Product(
          id: responseData['name'],
          title: title,
          description: description,
          image: image,
          price: price,
          location: location,
          isFavorite: false,
          userEmail: _authenticatedUser.email,
          userId: _authenticatedUser.id
      );
      _products.add(newProduct);
      _isLoading = false;
      notifyListeners();
      return true;
    } catch (error) {
      _isLoading = false;
      notifyListeners();
      return false;
    }
  }

  Future<bool> removeProduct(){
    _isLoading = true;
    final productId = selectedProduct.id;
    _products.removeAt(selectedProductIndex);
    _selectedProductId = null;
    notifyListeners();
    return http.delete('https://flutter-first-app-55155.firebaseio.com/products/$productId.json?auth=${_authenticatedUser.idToken}')
      .then((http.Response response) {
        _isLoading = false;
        notifyListeners();
        return true;
    }).catchError((error) {
      _isLoading = false;
      notifyListeners();
      return false;
    });
  }

  Future<Null> fetchProducts({bool triggerLoader = true, bool onlyForUser = false}){
    if (triggerLoader) {
      _isLoading = true;
      notifyListeners();
    }
    return http.get('https://flutter-first-app-55155.firebaseio.com/products.json?auth=${_authenticatedUser.idToken}')
      .then<Null>((http.Response response) {
        final List<Product> fetchProductList = [];
        final Map<String, dynamic> productListData = jsonDecode(response.body);
        if (productListData != null) {
          productListData.forEach((String productId, dynamic productData) {
            final Product product = Product(
                id: productId,
                title: productData['title'],
                description: productData['description'],
                image: productData['image'],
                price: productData['price'],
                location: Location(latitude: productData['loc_lat'], longitude: productData['loc_lng'], address: productData['loc_address']),
                isFavorite: productData['wishlistUsers'] == null ? false : (productData['wishlistUsers'] as Map<String, dynamic>).containsKey(_authenticatedUser.id),
                userEmail: productData['userEmail'],
                userId: productData['userId']
            );
            fetchProductList.add(product);
          });
        }
        if(onlyForUser){
          _products = fetchProductList.where((Product product){
            return product.userId == _authenticatedUser.id;
          }).toList();
        } else {
          _products = fetchProductList;
        }
        _isLoading = false;
        notifyListeners();
        _selectedProductId = null;
      }).catchError((error) {
      _isLoading = false;
      notifyListeners();
      return false;
    });
  }

  Future<bool> updateProduct({String title, String description, String image, double price, Location location,  bool isFavorite = false}){
    _isLoading = true;
    notifyListeners();
    final Map<String, dynamic> updateData = {
      'title': title,
      'description': description,
      'image': 'https://dummyimage.com/600x400/e0a2e0/1220e3.png',
      'price': price,
      'loc_lat': location.latitude,
      'loc_lng': location.longitude,
      'loc_address': location.address,
      'userEmail': selectedProduct.userEmail,
      'userId': selectedProduct.userId
    };
    return http.put('https://flutter-first-app-55155.firebaseio.com/products/${selectedProduct.id}.json?auth=${_authenticatedUser.idToken}',
      body: jsonEncode(updateData))
        .then((http.Response response) {
          final Map<String, dynamic> productData = jsonDecode(response.body);
          _products[selectedProductIndex] = Product(
            id: selectedProduct.id,
            title: productData['title'],
            description: productData['description'],
            image: productData['image'],
            price: productData['price'],
            location: location,
            isFavorite: productData['wishlistUsers'] == null ? false : (productData['wishlistUsers'] as Map<String, dynamic>).containsKey(_authenticatedUser.id),
            userEmail: selectedProduct.userEmail,
            userId: selectedProduct.userId,
          );
          _isLoading = false;
          notifyListeners();
          return true;
        }
      ).catchError((error) {
      _isLoading = false;
      notifyListeners();
      return false;
    });
  }

  void toggleProductFavoriteStatus() async {
    final bool newFavoriteStatus = !selectedProduct.isFavorite;
    _products[selectedProductIndex] = Product(
        id: selectedProduct.id,
        title: selectedProduct.title,
        description: selectedProduct.description,
        image: selectedProduct.image,
        price: selectedProduct.price,
        location: selectedProduct.location,
        userEmail: selectedProduct.userEmail,
        userId: selectedProduct.userId,
        isFavorite: newFavoriteStatus
    );
    http.Response response;
    if(newFavoriteStatus){
      response = await http.put('https://flutter-first-app-55155.firebaseio.com/products/${selectedProduct.id}/wishlistUsers/${_authenticatedUser.id}.json?auth=${_authenticatedUser.idToken}',
        body: jsonEncode(true)
      );
    } else {
      response = await http.delete('https://flutter-first-app-55155.firebaseio.com/products/${selectedProduct.id}/wishlistUsers/${_authenticatedUser.id}.json?auth=${_authenticatedUser.idToken}');
    }
    if (response.statusCode != 200 && response.statusCode != 201) {
      // On error roll back.
      _products[selectedProductIndex] = Product(
          id: selectedProduct.id,
          title: selectedProduct.title,
          description: selectedProduct.description,
          image: selectedProduct.image,
          price: selectedProduct.price,
          location: selectedProduct.location,
          userEmail: selectedProduct.userEmail,
          userId: selectedProduct.userId,
          isFavorite: !newFavoriteStatus
      );
    }
    notifyListeners();
  }

  void toggleDisplayMode() {
    _showFavorites = !_showFavorites;
    notifyListeners();
  }

  void selectProduct(String productId){
    _selectedProductId = productId;
    if (_selectedProductId != null) {
      notifyListeners();
    }
  }
}

mixin UtilityModule on ConnectedProductsModel{
  bool get isLoading {
    return _isLoading;
  }
}